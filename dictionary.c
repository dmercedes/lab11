#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Load the dictionary of words from the given filename.
// Return a pointer to the array of strings.
// Sets the value of size to be the number of valid
// entries in the array (not the total array length).
char ** loadDictionary(char *filename, int *size)
{
    // Allocate memory for an array of strings (arr).
    char ** textfile = malloc(10 * sizeof(char *));
	FILE *in = fopen(filename, "r");
    if (!in)
    {
        perror("Can't open file ");
        exit(1);
    }
    int count = 0;
    char line[50];
    // Read the dictionary line by line.
    while(fgets(line, 50, in) != NULL)
    {
        char *nl = strchr(line, '\n');
        if(nl) *nl = '\0';
        //textfile[count] = line;
        count++;
        if (count ==10)
        {
            // Expand array if necessary (realloc).
            fprintf(stderr,"Growing array\n");
            textfile = realloc(textfile, 20 *sizeof(char *));
        }
        //returns pointer to a chunk of memory
        char *textline = malloc(strlen(line) +1);
        
	    // Allocate memory for the string (str).
	    // Copy each line into the string (use strcpy).
        strcpy(textline, line);
        
        //add to array
        // Attach the string to the large array (assignment =).
        textfile[count] = textline;
        count++;
	
    }
	
	
	
	// The size should be the number of entries in the array.
	*size = count; //or 0
	//size = count;
	
    // Return pointer to the array of strings.
	return textfile;
}

// Search the dictionary for the target string
// Return the found string or NULL if not found.
char * searchDictionary(char *target, char **dictionary, int size)
{
    if (dictionary == NULL) return NULL;
    
	for (int i = 0; i < size; i++)
	{
	    if (strcmp(target, dictionary[i]) == 0)
	    {
	        return dictionary[i];
	    }
	}
	return NULL;
}